const { chai, chaiHttp, server } = require("./userTest");

chai.should();

chai.use(chaiHttp);
let token;

describe("login", () => {
  it("should login", (done) => {
    chai
      .request(server)
      .post("/loginUser")
      .send({
        email: "shaik@",
      })
      .end((err, res) => {
        token = res.body.token;
        res.should.have.status(200);
        done();
      });
  });
});

describe("GET /blogs/getAllBlogs", () => {
  console.log(token);
  it("should GET all BLOGS", (done) => {
    chai
      .request(server)
      .get("/users/getAllBlogs")
      .end((err, response) => {
        response.should.have.status(200);
        response.body.should.be.a("array");
        done();
      });
  });
});

describe("CREATE Blog", () => {
  //   it("it should create a user", (done) => {
  //     chai
  //       .request(server)
  //       .post("/blogs/createBlog")
  //       .set("authorization", "bearer " + token)
  //       .send({
  //         title: "Spider",
  //         content: "NO way HOme",
  //         Userid: "8",
  //       })
  //       .end((err, response) => {
  //         response.should.have.status(200);
  //         response.body.message.should.equal("Blog created Successfully");
  //         done();
  //       });
  //   });

  it("it should NOT  create a Blog(all fields are required)", (done) => {
    chai
      .request(server)
      .post("/blogs/createBlog")
      .set("authorization", "bearer " + token)
      .send({
        title: "Spider",
        Userid: 8,
      })
      .end((err, response) => {
        response.should.have.status(500);
        done();
      });
  });
});

describe(" GET /blogs/getBlogById/:id", () => {
  it("should GET a BLOG by ID", (done) => {
    const id = 14;
    chai
      .request(server)
      .get("/blogs/getBlogById/" + id)
      .set("authorization", "bearer " + token)
      .end((err, response) => {
        response.should.have.status(200);
        response.body.should.be.a("object");
        response.body.should.have.property("title");
        response.body.should.have.property("content");
        response.body.should.have.property("Userid");
        done();
      });
  });

  it("should NOT GET a BLOG by ID (invalid id)", (done) => {
    const id = 122;
    chai
      .request(server)
      .get("/blogs/getBlogById/" + id)
      .set("authorization", "bearer " + token)
      .end((err, response) => {
        response.should.have.status(404);
        response.body.message.should.equal("Invalid ID");
        done();
      });
  });
});

describe(" GET /blogs/getBlogByUserId/:id", () => {
  it("should GET a BLOG by USER ID", (done) => {
    const id = 8;
    chai
      .request(server)
      .get("/blogs/getBlogByUserId/" + id)
      .set("authorization", "bearer " + token)
      .end((err, response) => {
        response.should.have.status(200);
        response.body.should.be.a("array");
        done();
      });
  });

  it("should NOT GET a BLOG by USER ID (invalid id)", (done) => {
    const id = 122;
    chai
      .request(server)
      .get("/blogs/getBlogByUserId/" + id)
      .set("authorization", "bearer " + token)
      .end((err, response) => {
        response.should.have.status(404);
        response.body.message.should.equal("Invalid ID");
        done();
      });
  });
});

describe("PUT blogs/updateBlog", () => {
  it("UPDATE USER", (done) => {
    const id = 19;
    chai
      .request(server)
      .put("/blogs/updateBlogById/" + id)
      .set("authorization", "bearer " + token)
      .send({
        title: "SpiderMan",
        content: "NO way HOme",
      })
      .set("authorization", "bearer " + token)
      .end((err, response) => {
        response.should.have.status(200);
        response.body.message.should.equal("updated successfully");
        done();
      });
  });
});

describe("DELETE   blogs/deleteBlogById", () => {
  //   it("should DELETE USER", (done) => {
  //     const id = 11;
  //     chai
  //       .request(server)
  //       .delete("/blogs/deleteBlogById/" + id)
  //       .set("authorization", "bearer " + token)
  //       .end((err, response) => {
  //         response.should.has.status(200);
  //         response.body.message.should.equal("deleted successfully");
  //         done();
  //       });
  //   });
  it("should NOT  DELETE Blog", (done) => {
    const id = 100;
    chai
      .request(server)
      .delete("/blogs/deleteBlogById/" + id)
      .set("authorization", "bearer " + token)
      .end((err, response) => {
        response.should.has.status(404);
        response.body.message.should.equal("Invalid ID");
        done();
      });
  });
});
