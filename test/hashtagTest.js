const { chai, chaiHttp, server } = require("./userTest");

chai.should();

chai.use(chaiHttp);
let token;

describe("login", () => {
  it("should login", (done) => {
    chai
      .request(server)
      .post("/loginUser")
      .send({
        email: "shaik@",
      })
      .end((err, res) => {
        token = res.body.token;
        res.should.have.status(200);
        done();
      });
  });
});

describe("GET /hashtags/getAllHashtags", () => {
  console.log(token);
  it("should GET all Hashtags", (done) => {
    chai
      .request(server)
      .get("/hashtags/getAllHashtags")
      .end((err, response) => {
        response.should.have.status(200);
        response.body.should.be.a("array");
        done();
      });
  });
});

describe("CREATE Hahtag", () => {
  //   it("it should create  Hashtag", (done) => {
  //     chai
  //       .request(server)
  //       .post("/hashtags/createHashtag")
  //       .set("authorization", "bearer " + token)
  //       .send({
  //         hashtag: "Spider",
  //         blogid: 19,
  //       })
  //       .end((err, response) => {
  //         response.should.have.status(200);
  //         response.body.message.should.equal("Hashtag created Successfully");
  //         done();
  //       });
  //   });

  it("it should NOT  create a Hashtag(Inserting same hashtag again)", (done) => {
    chai
      .request(server)
      .post("/hashtags/createHashtag")
      .set("authorization", "bearer " + token)
      .send({
        hashtag: "Spider",
        blogid: 19,
      })
      .end((err, response) => {
        response.should.have.status(200);
        response.body.message.should.equal("Hashtag already exist");
        done();
      });
  });
});

describe("PUT hashtags/updateHashtagById", () => {
  it("UPDATE Hashtag", (done) => {
    const id = 13;
    chai
      .request(server)
      .put("/hashtags/updateHashtagById/" + id)
      .set("authorization", "bearer " + token)
      .send({
        hashtag: "Blogs",
      })
      .set("authorization", "bearer " + token)
      .end((err, response) => {
        response.should.have.status(200);
        response.body.message.should.equal("updated successfully");
        done();
      });
  });
});


describe("DELETE   blogs/deleteBlogById", () => {
    //   it("should DELETE USER", (done) => {
    //     const id = 11;
    //     chai
    //       .request(server)
    //       .delete("/hashtags/deleteHashtagById/" + id)
    //       .set("authorization", "bearer " + token)
    //       .end((err, response) => {
    //         response.should.has.status(200);
    //         response.body.message.should.equal("deleted successfully");
    //         done();
    //       });
    //   });
    it("should NOT  DELETE Blog", (done) => {
      const id = 100;
      chai
        .request(server)
        .delete("/hashtags/deleteHashtagById/" + id)
        .set("authorization", "bearer " + token)
        .end((err, response) => {
          response.should.has.status(404);
          response.body.message.should.equal("Invalid ID");
          done();
        });
    });
  });