const chai = require("chai");
const chaiHttp = require("chai-http");
const server = require("../server");

chai.should();

chai.use(chaiHttp);

let token;
describe("login", () => {
  it("should login", (done) => {
    chai
      .request(server)
      .post("/loginUser")
      .send({
        email: "shaik@",
      })
      .end((err, res) => {
        token = res.body.token;
        res.should.have.status(200);
        done();
      });
  });
});

describe("GET /users/getusers", () => {
  console.log(token);
  it("should GET all users", (done) => {
    chai
      .request(server)
      .get("/users/getAllUsers")
      .set("authorization", "bearer " + token)
      .end((err, response) => {
        response.should.have.status(200);
        response.body.should.be.a("array");
        done();
      });
  });
});

describe("CREATE USER", () => {
  //   it("it should create a user", (done) => {
  //     chai
  //       .request(server)
  //       .post("/users/createUser")
  //       .send({
  //         name: "sameer",
  //         email: "sameer@",
  //         phone_no:"989919191"
  //       })
  //       .end((err, response) => {
  //         response.should.have.status(200);
  //         response.body.message.should.equal("User created Successfully");
  //         done()
  //       });
  //   });
  it("it should NOT  create a user(email is required)", (done) => {
    chai
      .request(server)
      .post("/users/createUser")
      .send({
        name: "sameer",
        phone_no: "989919191",
      })
      .end((err, response) => {
        response.should.have.status(500);
        done();
      });
  });
});

describe(" GET /users/getUserById/:id", () => {
  it("should GET a USER by ID", (done) => {
    const id = 8;
    chai
      .request(server)
      .get("/users/getuserbyid/" + id)
      .set("authorization", "bearer " + token)
      .end((err, response) => {
        response.should.have.status(200);
        response.body.should.be.a("object");
        response.body.should.have.property("id");
        response.body.should.have.property("name");
        response.body.should.have.property("email");
        done();
      });
  });

  it("should NOT GET a USER by ID (invalid id)", (done) => {
    const id = 122;
    chai
      .request(server)
      .get("/users/getUserById/" + id)
      .set("authorization", "bearer " + token)
      .end((err, response) => {
        response.should.have.status(404);
        response.body.message.should.equal("Invalid ID");
        done();
      });
  });
});

describe("PUT users/updateUser", () => {
  it("UPDATE USER", (done) => {
    const id = 8;
    chai
      .request(server)
      .put("/users/updateUserById/" + id)
      .set("authorization", "bearer " + token)
      .send({
        name: "shaik sam",
        email: "shaik@",
        phone_no: "8989222",
      })
      .set("authorization", "bearer " + token)
      .end((err, response) => {
        response.should.have.status(200);
        response.body.message.should.equal("updated successfully");
        done();
      });
  });
});

describe("DELETE   users/deleteUserById", () => {
  //   it("should DELETE USER", (done) => {
  //     const id = 12;
  //     chai
  //       .request(server)
  //       .delete("/users/deleteUserById/"+id)
  //       .set("authorization", "bearer " + token)
  //       .end((err, response) => {
  //         response.should.has.status(200);
  //         response.body.message.should.equal("deleted successfully");
  //         done();
  //       });
  //   });
  it("should NOT  DELETE USER", (done) => {
    const id = 100;
    chai
      .request(server)
      .delete("/users/deleteUserById/" + id)
      .set("authorization", "bearer " + token)
      .end((err, response) => {
        response.should.has.status(404);
        response.body.message.should.equal("Invalid ID");
        done();
      });
  });
});

module.exports = { chai, chaiHttp, server };
