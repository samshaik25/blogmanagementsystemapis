const express = require("express");

require("dotenv").config();

const app = express();

app.use(express.json());

const userRouter = require("./src/routes/userRoutes");
app.use("/users", userRouter);

const blogRouter = require("./src/routes/blogsRoutes");
app.use("/blogs", blogRouter);

const hashtagRouter = require("./src/routes/hashtagRouter");
app.use("/hashtags", hashtagRouter);

const loginRoute = require("./src/routes/loginRoute");
app.use("/loginUser", loginRoute);

const port = process.env.PORT;

app.listen(port, () => {
  console.log(`listening on port ${port} `);
});

module.exports = app;
