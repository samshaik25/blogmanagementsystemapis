const {
  getUsersModel,
  createUserModel,
  getUserByIdModel,
  updateUserModel,
  deleteUserModel,
} = require("../models/userModel");

const { getOrSetCache } = require("../redis/getOrSetRedis");

module.exports = {
  getUsers: async (req, res) => {
    const data = await getOrSetCache("users", async () => {
      try {
        const result = await getUsersModel();
        return result;
      } catch (err) {
        res.status(500).json({
          message: err.message || "Some error occurred while getting users.",
        });
      }
    });
    res.send(data);
  },

  createUser: async (req, res) => {
    try {
      const body = req.body;
      await createUserModel(body);
      res.send({ message: "User created Successfully" });
    } catch (err) {
      res.status(500).json({
        message: err.message || "Some error occurred while creating user.",
      });
    }
  },

  getUserById: async (req, res) => {
    const data = await getOrSetCache(`user:${req.params.id}`, async () => {
      try {
        const id = req.params.id;
        const result = await getUserByIdModel(id);
        if (!result) {
          res.status(404).json({
            message: "Invalid ID",
          });
        }
        return result;
      } catch (err) {
        res.status(500).json({
          message: err.message || "Some error occurred.",
        });
      }
    });
    res.send(data);
  },

  updateUser: async (req, res) => {
    try {
      const id = req.params.id;
      const body = req.body;
      const result = await updateUserModel(id, body);
      if (!result) {
        return res.status(404).json({
          message: "Invalid ID",
        });
      }
      res.send({
        message: "updated successfully",
      });
    } catch (err) {
      res.status(500).json({
        message: err.message || "Some error occurred while updating.",
      });
    }
  },

  deleteUser: async (req, res) => {
    try {
      const id = req.params.id;
      const result = await deleteUserModel(id);
      if (!result) {
        return res.status(404).json({
          message: "Invalid ID",
        });
      }
      res.send({
        message: "deleted successfully",
      });
    } catch (err) {
      res.status(500).json({
        message: err.message || "Some error occurred while deleting",
      });
    }
  },
};
