const { sign } = require("jsonwebtoken");

const { loginUserModel } = require("../../src/models/loginModel");

module.exports = {
  loginUser: (req, res) => {
    const email = req.body.email;
    loginUserModel(email, (err, results) => {
      if (err) {
        console.log(err);
      }
      if (!results) {
        return res.json({
          message: "invalid email",
        });
      }
      const accessToken = sign({ results }, process.env.secretKey, {
        expiresIn: "24h",
      });
      return res.send({
        token: accessToken,
      });
    });
  },
};
