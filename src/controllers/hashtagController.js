const {
  getHashtagModel,
  createHashtagModel,
  updateHashtagModel,
  deleteHashtagModel,
} = require("../models/hashtagModel");

module.exports = {
  getHashtags: async (req, res) => {
    try {
      const result = await getHashtagModel();
      res.send(result);
    } catch (err) {
      res.status(500).json({
        message: err.message || "Some error occurred while getting users.",
      });
    }
  },

  createHashtags: async (req, res) => {
    try {
      const body = req.body;
      const result = await createHashtagModel(body);
      if (result == "hashtag exist") {
        res.send({ message: "Hashtag already exist" });
      } else if (result == "No Blog with that ID") {
        res.send({ message: "No Blog with that ID" });
      } else {
        res.send({ message: "Hashtag created Successfully" });
      }
    } catch (err) {
      res.status(500).json({
        message: err.message || "Some error occurred while creating Hashtag.",
      });
    }
  },

  updateHashtag: async (req, res) => {
    try {
      const id = req.params.id;
      const body = req.body;
      const result = await updateHashtagModel(id, body);
      if (!result) {
        return res.status(404).json({
          message: "Invalid ID",
        });
      } else {
        res.send({
          message: "updated successfully",
        });
      }
    } catch (err) {
      res.status(500).json({
        message: err.message || "Some error occurred while updating.",
      });
    }
  },

  deleteHashtag: async (req, res) => {
    try {
      const id = req.params.id;
      const result = await deleteHashtagModel(id);
      if (!result) {
        res.status(404).json({
          message: "Invalid ID",
        });
      }
      res.send({
        message: "deleted successfully",
      });
    } catch (err) {
      res.status(500).json({
        message: err.message || "Some error occurred while deleting",
      });
    }
  },
};
