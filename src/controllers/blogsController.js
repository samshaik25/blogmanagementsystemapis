const {
  getBlogsModel,
  createBlogsModel,
  getBlogByIdModel,
  getBlogByUserIdModel,
  getBlogByHashtagModel,
  updateBlogModel,
  deleteBlogModel,
} = require("../models/blogsModel");

const { getOrSetCache } = require("../redis/getOrSetRedis");

controller = {
  getBlogs: async (req, res) => {
    const data = await getOrSetCache("blogs", async () => {
      try {
        const results = await getBlogsModel();
        return results;
      } catch (error) {
        return res.status(500).json({
          message: error.message || "Some error occurred while getting users.",
        });
      }
    });
    res.send(data);
  },

  createBlogs: async (req, res) => {
    try {
      const body = req.body;
      const result = await createBlogsModel(body);
      res.send({ message: "Blog created Successfully" });
    } catch (err) {
      res.status(500).json({
        message: err.message || "Some error occurred while creating user.",
      });
    }
  },

  getBlogById: async (req, res) => {
    const data = await getOrSetCache(
      `blogById:${req.params.id}`,
      async () => {
        try {
          const id = req.params.id;
          const result = await getBlogByIdModel(id);
          if (!result) {
            res.status(404).send({
              message: "Invalid ID",
            });
          }
          res.send(result);
        } catch (err) {
          res.status(500).json({
            message: err.message || "Some error occurred.",
          });
        }
      }
    );
  },

  getBlogByUserId: async (req, res) => {
    const data = await getOrSetCache(
      `blogByUserId:${req.params.id}`,
      async () => {
        try {
          const id = req.params.id;
          const result = await getBlogByUserIdModel(id);
          if (!result) {
            res.status(404).send({
              message: "Invalid ID",
            });
          }
          return result;
        } catch (err) {
          res.status(500).json({
            message: err.message || "Some error occurred.",
          });
        }
      }
    );
    res.send(data);
  },

  getBlogByHashtag: async (req, res) => {
    const data = await getOrSetCache(`hashtag:${req.params.hash}`, async () => {
      try {
        const hash = req.params.hash;
        const result = await getBlogByHashtagModel(hash);
        if (!result) {
          res.status(404).send({
            message: "Invalid Hashtag",
          });
        }
        return result;
      } catch (err) {
        res.status(500).json({
          message: err.message || "Some error occurred.",
        });
      }
    });
    res.send(data);
  },
  
  updateBlog: async (req, res) => {
    try {
      const id = req.params.id;
      const body = req.body;
      const result = await updateBlogModel(id, body);
      if (!result) {
        return res.status(404).json({
          message: "Invalid ID",
        });
      }
      res.send({
        message: "updated successfully",
      });
    } catch (err) {
      res.status(500).json({
        message: err.message || "Some error occurred while updating.",
      });
    }
  },

  deleteBlog: async (req, res) => {
    try {
      const id = req.params.id;
      const result = await deleteBlogModel(id);
      if (!result) {
        res.status(404).json({
          message: "Invalid ID",
        });
      }
      res.send({
        message: "deleted successfully",
      });
    } catch (err) {
      res.status(500).json({
        message: err.message || "Some error occurred while deleting",
      });
    }
  },
};





module.exports=controller
