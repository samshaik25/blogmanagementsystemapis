const router = require("./masterRoute");
const { checkToken } = require("../middleware/validate");

const {
  getBlogs,
  createBlogs,
  getBlogById,
  getBlogByUserId,
  getBlogByHashtag,
  updateBlog,
  deleteBlog,
} = require("../controllers/blogsController");

router.get("/getAllBlogs", getBlogs);

router.post("/createBlog", checkToken, createBlogs);

router.get("/getBlogHashtag/:hash", checkToken, getBlogByHashtag);

router.get("/getBlogById/:id", checkToken, getBlogById);

router.get("/getBlogByUserId/:id", checkToken, getBlogByUserId);

router.put("/updateBlogById/:id", checkToken, updateBlog);

router.delete("/deleteBlogById/:id", checkToken, deleteBlog);

module.exports = router;
