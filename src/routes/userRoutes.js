const router = require("./masterRoute");
const { checkToken } = require("../middleware/validate");

const {
  getUsers,
  createUser,
  getUserById,
  updateUser,
  deleteUser,
} = require("../controllers/userController");

router.post("/createUser", createUser);

router.get("/getUserById/:id", checkToken, getUserById);

router.get("/getAllUsers", checkToken, getUsers);

router.put("/updateUserById/:id", checkToken, updateUser);

router.delete("/deleteUserById/:id", checkToken, deleteUser);

module.exports = router;
