const router = require("./masterRoute");
const { checkToken } = require("../middleware/validate");

const {
  getHashtags,
  createHashtags,
  updateHashtag,
  deleteHashtag,
} = require("../controllers/hashtagController");

router.get("/getAllHashtags", getHashtags);

router.post("/createHashtag", checkToken, createHashtags);

router.put("/updateHashtagById/:id", checkToken, updateHashtag);

router.delete("/deleteHashtagById/:id", checkToken, deleteHashtag);

module.exports = router;
