const pool = require("../../config/dbConfig");

module.exports = {
  getUsersModel: () => {
    return new Promise((resolve, reject) => {
      pool.query(`select * from User`, (err, results) => {
        if (err) {
          reject(err);
        }
        resolve(results);
      });
    });
  },

  createUserModel: (data) => {
    return new Promise((resolve, reject) => {
      pool.query(
        `INSERT into User (name,email,phone_no) values(?,?,?)`,
        [data.name, data.email, data.phone_no],
        (err, results) => {
          if (err) {
            reject(err);
          }
          resolve(results);
        }
      );
    });
  },

  getUserByIdModel: (id) => {
    return new Promise((resolve, reject) => {
      pool.query(`select * from User where id=?`, [id], (err, results) => {
        if (err) {
          reject(err);
        }
        if (results[0] == undefined) {
        return  resolve(undefined);
        }
        resolve(results[0]);
      });
    });
  },

  updateUserModel: (id, data) => {
    return new Promise((resolve, reject) => {
      pool.query(
        `update User set name=?,email=?,  phone_no=? where id=?`,
        [data.name, data.email, data.phone_no, id],
        (err, results) => {
          if (err) {
            reject(err);
          }
          resolve(results.affectedRows);
        }
      );
    });
  },

  deleteUserModel: (id) => {
    return new Promise((resolve, reject) => {
      pool.query(`delete from User where id=?`, [id], (err, results) => {
        if (err) {
          reject(err);
        }
        resolve(results.affectedRows);
      });
    });
  },
};
