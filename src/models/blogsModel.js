const pool = require("../../config/dbConfig");

module.exports = {
  getBlogsModel: () => {
    return new Promise((resolve, reject) => {
      pool.query(`select * from Blogs`, (err, results) => {
        if (err) {
          reject(err);
        }
        resolve(results);
      });
    });
  },

  createBlogsModel: (data) => {
    return new Promise((resolve, reject) => {
      pool.query(
        `INSERT into Blogs(title,content,Userid) values(?,?,?)`,
        [data.title, data.content, data.Userid],
        (err, results) => {
          if (err) {
            reject(err);
          }
          resolve(results);
        }
      );
    });
  },

  getBlogByIdModel: (id) => {
    return new Promise((resolve, reject) => {
      pool.query(`select * from Blogs where id=?`, [id], (err, results) => {
        if (err) {
          reject(err);
        }
        if (results[0] == undefined) {
          resolve(undefined);
        }
        resolve(results[0]);
      });
    });
  },

  getBlogByUserIdModel: (userId) => {
    return new Promise((resolve, reject) => {
      pool.query(
        `select * from Blogs where Userid=?`,
        [userId],
        (err, results) => {
          if (err) {
            reject(err);
          }
          if (results[0] == undefined) {
            resolve(undefined);
          }
          resolve(results);
        }
      );
    });
  },

  getBlogByHashtagModel: (hash) => {
    return new Promise((resolve, reject) => {
      pool.query(
        `select Blogs.id as Blog_Id, Blogs.title as title,Blogs.content as content,Hashtags.hashtag as hashtag from Blogs 
       INNER JOIN Hashtags  ON Blogs.id=Hashtags.blogid where hashtag=?`,
        [hash],

        (err, results) => {
          if (err) {
            reject(err);
          }
          console.log(results);
          if (results[0] == undefined) {
            resolve(undefined);
          }
          resolve(results);
        }
      );
    });
  },

  updateBlogModel: (id, data) => {
    return new Promise((resolve, reject) => {
      pool.query(
        `update Blogs set title=?,content=? where id=?`,
        [data.title, data.content, id],
        (err, results) => {
          if (err) {
            reject(err);
          }
          resolve(results.affectedRows);
        }
      );
    });
  },

  deleteBlogModel: (id) => {
    return new Promise((resolve, reject) => {
      pool.query(`delete from Blogs where id=?`, [id], (err, results) => {
        if (err) {
          reject(err);
        }
        resolve(results.affectedRows);
      });
    });
  },
};
