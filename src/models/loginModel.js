const pool = require("../../config/dbConfig");

module.exports = {
  loginUserModel: (email, callback) => {
    pool.query(`select * from User where email=? `, [email], (err, results) => {
      if (err) {
        return callback(err);
      }
      return callback(null, results[0]);
    });
  },
};
