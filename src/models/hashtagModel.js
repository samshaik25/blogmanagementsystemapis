const pool = require("../../config/dbConfig");
const { getBlogsModel } = require("./blogsModel");

hashtagModels = {
  getHashtagModel: () => {
    return new Promise((resolve, reject) => {
      pool.query(`select * from Hashtags`, (err, results) => {
        if (err) {
          reject(err);
        }
        resolve(results);
      });
    });
  },

  createHashtagModel: async (data) => {
    return new Promise(async (resolve, reject) => {
      const Blog = await getBlogsModel();
      const checkBlogId = Blog.filter((res) => res.id == data.blogid);
      if (checkBlogId[0] != undefined) {
        const hashtag = await hashtagModels.getHashtagModel();
        const checkHashtag = hashtag.filter(
          (res) => res.hashtag == data.hashtag && res.blogid == data.blogid
        );
        if (checkHashtag[0] == undefined) {
          pool.query(
            `INSERT into Hashtags(hashtag,blogid) values(?,?)`,
            [data.hashtag, data.blogid],
            (err, results) => {
              if (err) {
                reject(err);
              }
              resolve(results);
            }
          );
        } else {
          resolve("hashtag exist");
        }
      } else {
        resolve("No Blog with that ID");
      }
    });
  },

  updateHashtagModel: (id, data) => {
    return new Promise((resolve, reject) => {
      pool.query(
        `update Hashtags set hashtag=? where id=?`,
        [data.hashtag, id],
        (err, results) => {
          if (err) {
            reject(err);
          }
          resolve(results.affectedRows);
        }
      );
    });
  },

  deleteHashtagModel: (id) => {
    return new Promise((resolve, reject) => {
      pool.query(`delete from Hashtags where id=?`, [id], (err, results) => {
        if (err) {
          reject(err);
        }
        resolve(results.affectedRows);
      });
    });
  },
};

module.exports = hashtagModels;
