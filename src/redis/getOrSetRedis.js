const Redis = require("redis");
const redisClient = Redis.createClient();
const DEFAULT_EXPIRATION = 30;

function getOrSetCache(key, cb) {
  return new Promise((resolve, reject) => {
    redisClient.get(key, async (error, data) => {
      if (error) return reject(error);
      if (data != null) {
        console.log("redis");
        return resolve(JSON.parse(data));
      }
      const freshData = await cb();
      if (freshData == undefined) return;

      redisClient.setex(key, DEFAULT_EXPIRATION, JSON.stringify(freshData));
      resolve(freshData);
    });
  });
}

module.exports = { getOrSetCache };
